﻿namespace Checkout.PaymentGatewayService.Data.Repository.AutoMapper
{
    using Checkout.PaymentGatewayService.Data.Repository.Entities;
    using Checkout.PaymentGatewayService.Domain.Model.Enums;
    using Checkout.PaymentGatewayService.Domain.Model.ValueObjects;
    using global::AutoMapper;

    public class DataToDomainMappingProfile : Profile
    {
        public DataToDomainMappingProfile()
        {
            CreateMap<PaymentTransaction, Domain.Model.Entities.PaymentTransaction>()
                .ForMember(s => s.CardDetails, d => d.MapFrom(t => new CardDetails(t.CardNumber, t.CardHolderName, t.CardExpirationMonth, t.CardExpirationYear, t.CardSecurityCode)))
                .ForMember(s => s.CurrencyCode, d => d.MapFrom(t => new ThreeLetterCurrencyCode(t.CurrencyCode)))
                .ForMember(s => s.Status, d => d.MapFrom(t => (PaymentStatus)t.Status))
                .ForMember(s => s.CreatedOn, d => d.MapFrom(t => t.DateCreated))
                .ForMember(s => s.UpdatedOn, d => d.MapFrom(t => t.DateUpdated));
        }
    }
}
