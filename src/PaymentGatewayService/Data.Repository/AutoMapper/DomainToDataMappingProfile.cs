﻿namespace Checkout.PaymentGatewayService.Data.Repository.AutoMapper
{
    using Checkout.PaymentGatewayService.Data.Repository.Entities;
    using global::AutoMapper;

    public class DomainToDataMappingProfile : Profile
    {
        public DomainToDataMappingProfile()
        {
            CreateMap<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>()
                .ForMember(s => s.CardNumber, d => d.MapFrom(t => t.CardDetails.Number))
                .ForMember(s => s.CardHolderName, d => d.MapFrom(t => t.CardDetails.HolderName))
                .ForMember(s => s.CardExpirationMonth, d => d.MapFrom(t => t.CardDetails.ExpirationMonth))
                .ForMember(s => s.CardExpirationYear, d => d.MapFrom(t => t.CardDetails.ExpirationYear))
                .ForMember(s => s.CardSecurityCode, d => d.MapFrom(t => t.CardDetails.SecurityCode))
                .ForMember(s => s.CurrencyCode, d => d.MapFrom(t => t.CurrencyCode.ToString()))
                .ForMember(s => s.Status, d => d.MapFrom(t => (int)t.Status))
                .ForMember(s => s.DateCreated, d => d.MapFrom(t => t.CreatedOn))
                .ForMember(s => s.DateUpdated, d => d.MapFrom(t => t.UpdatedOn));
        }
    }
}
