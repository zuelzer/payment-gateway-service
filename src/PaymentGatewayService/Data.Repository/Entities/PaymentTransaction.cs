﻿namespace Checkout.PaymentGatewayService.Data.Repository.Entities
{
    using System;

    public class PaymentTransaction
    {
        public Guid Id { get; set; }

        public string MerchantId { get; set; }

        public string CardNumber { get; set; }

        public string CardHolderName { get; set; }

        public int CardExpirationMonth { get; set; }

        public int CardExpirationYear { get; set; }

        public string CardSecurityCode { get; set; }

        public string CurrencyCode { get; set; }

        public decimal Amount { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }
    }
}
