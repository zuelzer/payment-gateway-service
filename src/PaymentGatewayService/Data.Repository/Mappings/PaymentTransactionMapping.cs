﻿namespace Checkout.PaymentGatewayService.Data.Repository.Mappings
{
    using Checkout.PaymentGatewayService.Data.Repository.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class PaymentTransactionMapping : IEntityTypeConfiguration<PaymentTransaction>
    {
        public void Configure(EntityTypeBuilder<PaymentTransaction> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.MerchantId).HasColumnName("MerchantId").HasMaxLength(20).IsRequired();
            builder.Property(t => t.CardNumber).HasColumnName("CardNumber").HasMaxLength(50).IsRequired();
            builder.Property(t => t.CardHolderName).HasColumnName("CardHolderName").HasMaxLength(50);
            builder.Property(c => c.CardExpirationMonth).HasColumnName("CardExpirationMonth");
            builder.Property(c => c.CardExpirationYear).HasColumnName("CardExpirationYear");
            builder.Property(t => t.CardSecurityCode).HasColumnName("CardSecurityCode").HasMaxLength(5).IsRequired();
            builder.Property(t => t.CurrencyCode).HasColumnName("CurrencyCode").HasMaxLength(3).IsRequired();
            builder.Property(c => c.Amount).HasColumnName("Amount").HasColumnType("decimal(9, 2)").IsRequired();
            builder.Property(c => c.Status).HasColumnName("Status");
            builder.Property(t => t.Message).HasColumnName("Message").HasMaxLength(255);
            builder.Property(t => t.DateCreated).HasColumnName("DateCreated").IsRequired();
            builder.Property(t => t.DateUpdated).HasColumnName("DateUpdated");

            builder.ToTable("PaymentTransactions");
        }
    }
}
