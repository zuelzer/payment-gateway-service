﻿namespace Checkout.PaymentGatewayService.Data.Repository.Context
{
    using System.Linq;
    using Checkout.PaymentGatewayService.Data.Repository.Entities;
    using Checkout.PaymentGatewayService.Data.Repository.Mappings;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;
    using Microsoft.EntityFrameworkCore.Metadata.Internal;

    public class SqlServerContext : DbContext
    {
        private readonly EntityFrameworkConfiguration _entityFrameworkConfiguration;

        public SqlServerContext(EntityFrameworkConfiguration entityFrameworkConfiguration)
            : base()
        {
            this._entityFrameworkConfiguration = entityFrameworkConfiguration;
        }

        public DbSet<PaymentTransaction> PaymentTransactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.RegisterConventions(modelBuilder);
            this.AddEntityConfigurations(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(this._entityFrameworkConfiguration.ToString());
            }
        }

        private void RegisterConventions(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(string)))
            {
                property.AsProperty().Builder.IsUnicode(false, ConfigurationSource.Convention);
            }
        }

        private void AddEntityConfigurations(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PaymentTransactionMapping());
        }
    }
}
