﻿namespace Checkout.PaymentGatewayService.Data.Repository.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Data.Repository.Context;
    using Checkout.PaymentGatewayService.Data.Repository.Entities;
    using Checkout.PaymentGatewayService.Domain.Core.Repositories;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;

    public class PaymentTransactionRepository : IPaymentTransactionRepository
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly Func<SqlServerContext> _factory;

        public PaymentTransactionRepository(ILoggerFactory loggerFactory, IMapper mapper, Func<SqlServerContext> factory)
        {
            this._logger = loggerFactory.CreateLogger<PaymentTransactionRepository>();
            this._mapper = mapper;
            this._factory = factory;
        }

        public async Task<Domain.Model.Entities.PaymentTransaction> GetAsync(string merchantId, Guid paymentTransactionId)
        {
            try
            {
                using(var dbContext = this._factory.Invoke())
                {
                    var paymentTransaction = await dbContext.PaymentTransactions.AsNoTracking().FirstOrDefaultAsync(pt => pt.Id == paymentTransactionId && pt.MerchantId == merchantId).ConfigureAwait(false);

                    if (paymentTransaction.IsNotNull())
                    {
                        return this._mapper.Map<PaymentTransaction, Domain.Model.Entities.PaymentTransaction>(paymentTransaction);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to get a transaction from database: merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId}", merchantId, paymentTransactionId);
            }

            return null;
        }

        public async Task InsertAsync(Domain.Model.Entities.PaymentTransaction paymentTransaction)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataPaymentTransaction = this._mapper.Map<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>(paymentTransaction);

                    dbContext.Add(dataPaymentTransaction);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to save a transaction into the database: merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId}", paymentTransaction.MerchantId, paymentTransaction.Id);
            }
        }

        public async Task UpdateAsync(Domain.Model.Entities.PaymentTransaction paymentTransaction)
        {
            try
            {
                using (var dbContext = this._factory.Invoke())
                {
                    var dataPaymentTransaction = this._mapper.Map<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>(paymentTransaction);

                    dbContext.Update(dataPaymentTransaction);

                    await dbContext.SaveChangesAsync().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to save a transaction into the database: merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId}", paymentTransaction.MerchantId, paymentTransaction.Id);
            }
        }
    }
}
