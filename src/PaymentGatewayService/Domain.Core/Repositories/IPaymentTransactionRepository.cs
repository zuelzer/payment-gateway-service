﻿namespace Checkout.PaymentGatewayService.Domain.Core.Repositories
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Domain.Model.Entities;

    public interface IPaymentTransactionRepository
    {
        Task InsertAsync(PaymentTransaction paymentTransaction);

        Task UpdateAsync(PaymentTransaction paymentTransaction);

        Task<PaymentTransaction> GetAsync(string merchantId, Guid paymentTransactionId);
    }
}
