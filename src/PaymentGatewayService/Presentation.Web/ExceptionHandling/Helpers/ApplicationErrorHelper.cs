﻿namespace Checkout.PaymentGatewayService.Presentation.Web.ExceptionHandling.Helpers
{
    using System;

    public static class ApplicationErrorHelper
    {
        public static ApplicationError GetApplicationErrorFromException(Exception exception)
        {
            if (exception.InnerException != null)
            {
                return GetApplicationErrorFromException(exception.InnerException);
            }
            else
            {
                return new ApplicationError
                {
                    StatusCode = HttpStatusCodeHelper.GetHttpStatusForException(exception),
                    Message = exception.Message
                };
            }
        }
    }
}
