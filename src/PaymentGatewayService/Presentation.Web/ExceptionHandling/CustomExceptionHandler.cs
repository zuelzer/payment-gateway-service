﻿namespace Checkout.PaymentGatewayService.Presentation.Web.ExceptionHandling
{
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Presentation.Web.ExceptionHandling.Helpers;
    using Microsoft.AspNetCore.Diagnostics;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using Newtonsoft.Json;

    public static class CustomExceptionHandler
    {
        public static Task HandleExceptionAsync(ILoggerFactory loggerFactory, HttpContext context)
        {
            var logger = loggerFactory.CreateLogger("CustomExceptionHandler");

            var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
            var exception = exceptionHandlerPathFeature.Error;

            var applicationError = ApplicationErrorHelper.GetApplicationErrorFromException(exception);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)applicationError.StatusCode;

            if ((int)applicationError.StatusCode < 500)
            {
                logger.LogWarning(exception, applicationError.Message, applicationError.StatusCode);
            }
            else
            {
                logger.LogError(exception, applicationError.Message, applicationError.StatusCode);
            }

            var response = JsonConvert.SerializeObject(applicationError);

            return context.Response.WriteAsync(response);
        }
    }
}
