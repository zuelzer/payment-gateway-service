namespace Checkout.PaymentGatewayService.Presentation.Web
{
    using System;
    using System.Linq;
    using AutoMapper;
    using Checkout.PaymentGatewayService.Application.Services.AutoMapper;
    using Checkout.PaymentGatewayService.Data.Gateway.AutoMapper;
    using Checkout.PaymentGatewayService.Data.Repository.AutoMapper;
    using Checkout.PaymentGatewayService.Data.Repository.Context;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Checkout.PaymentGatewayService.Presentation.Bootstrapper;
    using Checkout.PaymentGatewayService.Presentation.Web.Configuration;
    using Checkout.PaymentGatewayService.Presentation.Web.ExceptionHandling;
    using Checkout.PaymentGatewayService.Presentation.Web.Extensions;
    using HealthChecks.UI.Client;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Diagnostics.HealthChecks;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Formatters;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Diagnostics.HealthChecks;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Serilog;

    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("conf/appsettings.json", optional: false, reloadOnChange: false)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var bankDependecyConfiguration = this.Configuration.GetConfiguration<BankDependecyConfiguration>();
            var entityFrameworkConfiguration = this.Configuration.GetConfiguration<EntityFrameworkConfiguration>();
            var healthCheckConfiguration = this.Configuration.GetConfiguration<HealthCheckConfiguration>();
            var loggingConfiguration = this.Configuration.GetConfiguration<LoggingConfiguration>();
            var swaggerConfiguration = this.Configuration.GetConfiguration<SwaggerConfiguration>();

            NativeInjectorBootstrapper.RegisterServices(services, bankDependecyConfiguration, entityFrameworkConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);

            services
                .AddSingleton(this.Configuration)
                .AddOptions()
                .AddSwaggerEnabled(swaggerConfiguration);

            services.AddMvc(opt =>
            {
                opt.UseCentralRoutePrefix(new RouteAttribute("PaymentGateway/v1/{merchantId}"));

                opt.EnableEndpointRouting = false;

                var noContentFormatter = opt.OutputFormatters.OfType<HttpNoContentOutputFormatter>().FirstOrDefault();

                if (noContentFormatter != null)
                {
                    noContentFormatter.TreatNullValueAsNoContent = false;
                }
            });

            services.AddHealthChecks()
                .AddDbContextCheck<SqlServerContext>("Infrastructure Dependency - SQL Server")
                .AddUrlGroup(new Uri(bankDependecyConfiguration.BaseEndPointUrl), name: $"Service Dependency - Bank: {bankDependecyConfiguration.BaseEndPointUrl}", failureStatus: (bankDependecyConfiguration.Mocked ? HealthStatus.Degraded : HealthStatus.Unhealthy))
                .AddUrlGroup(new Uri(loggingConfiguration.ApplicationInsightUrl), name: $"Monitoring Dependency : Seq Logging: {loggingConfiguration.ApplicationInsightUrl}", failureStatus: HealthStatus.Degraded);

            services.AddHealthChecksUI()
                .AddInMemoryStorage();

            services.AddAutoMapper(typeof(DomainToApplicationMappingProfile), typeof(ApplicationToDomainMappingProfile), typeof(DomainToDataMappingProfile), typeof(DataToDomainMappingProfile), typeof(ApplicationToGatewayMappingProfile));

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, HealthCheckConfiguration healthCheckConfiguration)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHealthChecks(healthCheckConfiguration.DefaultPath, new HealthCheckOptions
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHealthChecksUI(options =>
            {
                options.UIPath = healthCheckConfiguration.UIPath;
                options.ApiPath = healthCheckConfiguration.ApiPath;
            });

            app.UseExceptionHandler(a => a.Run(async context => await CustomExceptionHandler.HandleExceptionAsync(loggerFactory, context)));

            app.UseSwaggerEnabled();

            app.UseSerilogRequestLogging();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "api/{controller}/{id?}");
            });
        }
    }
}
