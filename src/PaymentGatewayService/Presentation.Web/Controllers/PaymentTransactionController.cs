﻿namespace Checkout.PaymentGatewayService.Presentation.Web.Controllers
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Application.DTO.Request;
    using Checkout.PaymentGatewayService.Application.Services.Interfaces;
    using Microsoft.AspNetCore.Mvc;

    [ApiController]
    public class PaymentTransactionController : ControllerBase
    {
        private readonly IPaymentTransactionAppService _paymentTransactionAppService;

        public PaymentTransactionController(IPaymentTransactionAppService paymentTransactionAppService)
        {
            this._paymentTransactionAppService = paymentTransactionAppService;
        }

        /// <summary>
        /// Get payment transaction details
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="paymentTransactionId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("PaymentTransactions/{paymentTransactionId:guid}", Name = "GetPaymentTransaction")]
        [ProducesResponseType(typeof(Application.DTO.Response.PaymentTransaction), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPaymentTransactionAsync([FromRoute] string merchantId, Guid paymentTransactionId)
        {
            var result = await this._paymentTransactionAppService.GetPaymentTransactionAsync(merchantId, paymentTransactionId).ConfigureAwait(false);

            return this.Ok(result);
        }

        /// <summary>
        /// Create a payment transaction
        /// </summary>
        /// <param name="merchantId"></param>
        /// <param name="paymentTransaction"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PaymentTransactions")]
        [ProducesResponseType(typeof(Application.DTO.Response.PaymentTransaction), (int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreatePaymentTransactionAsync([FromRoute] string merchantId, [FromBody] PaymentTransaction paymentTransaction)
        {
            var result = await this._paymentTransactionAppService.CreatePaymentTransactionAsync(merchantId, paymentTransaction).ConfigureAwait(false);   
            
            return this.CreatedAtRoute("GetPaymentTransaction", new { paymentTransactionId = result.Id }, result);
        }
    }
}
