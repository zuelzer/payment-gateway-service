﻿namespace Checkout.PaymentGatewayService.Application.DTO.Response
{
    using System;

    public class PaymentTransaction
    {
        public Guid Id { get; set; }

        public string Status { get; set; }

        public string Message { get; set; }

        public DateTime? ProcessedOn { get; set; }

        public CardDetails CardDetails { get; set; }

        public string CurrencyCode { get; set; }

        public decimal Amount { get; set; }
    }
}
