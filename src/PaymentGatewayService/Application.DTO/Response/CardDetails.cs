﻿namespace Checkout.PaymentGatewayService.Application.DTO.Response
{
    public class CardDetails
    {
        public string ObfuscatedNumber { get; set; }

        public string HolderName { get; set; }

        public int ExpirationMonth { get; set; }

        public int ExpirationYear { get; set; }
    }
}
