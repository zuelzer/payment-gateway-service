﻿namespace Checkout.PaymentGatewayService.Application.DTO.Request
{
    public class CardDetails
    {
        public string Number { get; set; }

        public string HolderName { get; set; }

        public int ExpirationMonth { get; set; }

        public int ExpirationYear { get; set; }

        public string SecurityCode { get; set; }
    }
}
