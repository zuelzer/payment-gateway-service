﻿namespace Checkout.PaymentGatewayService.Application.DTO.Request
{
    public class PaymentTransaction
    {
        public CardDetails CardDetails { get; set; }

        public string CurrencyCode { get; set; }

        public decimal Amount { get; set; }
    }
}
