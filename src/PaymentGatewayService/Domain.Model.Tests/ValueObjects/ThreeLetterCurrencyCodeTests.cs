﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects.Tests
{
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class ThreeLetterCurrencyCodeTests
    {
        private const string currencyCode = "EUR";

        [Fact]
        public void Constructor_ValidCurrencyCode_NewThreeLetterCurrencyCodeWithFilledValue()
        {
            // Arrange
            var expectedValue = "EUR";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(currencyCode);

            // Assert
            Assert.NotNull(threeLetterCurrencyCode);
            Assert.Equal(expectedValue, threeLetterCurrencyCode.Value);
        }

        [Fact]
        public void Constructor_ValidLowerCaseCurrencyCode_NewThreeLetterCurrencyCodeWithFilledValue()
        {
            // Arrange
            var currencyCode = "eur";
            var expectedValue = "EUR";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(currencyCode);

            // Assert
            Assert.NotNull(threeLetterCurrencyCode);
            Assert.Equal(expectedValue, threeLetterCurrencyCode.Value);
        }

        [Fact]
        public void ToString_ValidCurrencyCode_ReturnsTwoLetterCountryCodeValue()
        {
            // Arrange
            var expectedValue = "EUR";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(currencyCode).ToString();

            // Assert
            Assert.NotNull(threeLetterCurrencyCode);
            Assert.Equal(expectedValue, threeLetterCurrencyCode);
        }

        [Fact]
        public void ToString_ValidLowerCaseCurrencyCode_ReturnsTwoLetterCountryCodeValue()
        {
            // Arrange
            var currencyCode = "eur";
            var expectedValue = "EUR";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(currencyCode).ToString();

            // Assert
            Assert.NotNull(threeLetterCurrencyCode);
            Assert.Equal(expectedValue, threeLetterCurrencyCode);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_NullOrEmptyCurrencyCode_ThrowsInvalidDomainValueObjectException(string value)
        {
            // Arrange
            var expectedMessage = "Currency code cannot be null or empty";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(value);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => threeLetterCurrencyCode.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("EU")]
        [InlineData("EURO")]
        public void Validate_NotThreeLetterCurrencyCode_ThrowsInvalidDomainValueObjectException(string value)
        {
            // Arrange
            var expectedMessage = "Currency code should have three letters";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(value);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => threeLetterCurrencyCode.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("AUD")]
        [InlineData("BRL")]
        [InlineData("JPY")]
        [InlineData("CNY")]
        public void Validate_UnknownCurrencyCode_ThrowsInvalidDomainValueObjectException(string value)
        {
            // Arrange
            var expectedMessage = "Currency code is not valid";

            // Act
            var threeLetterCurrencyCode = new ThreeLetterCurrencyCode(value);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => threeLetterCurrencyCode.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Contains(expectedMessage, exception.Message);
        }
    }
}
