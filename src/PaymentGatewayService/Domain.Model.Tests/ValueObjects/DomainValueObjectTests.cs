﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects.Tests
{
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class DomainValueObjectTests
    {
        [Fact]
        public void EqualityOverloadedOperator_ValueObjectsSameInstance_ReturnsTrue()
        {
            FakeDomainValueObject left = new FakeDomainValueObject();
            FakeDomainValueObject right = left;

            var result = left == right;

            Assert.True(result);
        }

        [Fact]
        public void EqualityOverloadedOperator_BothValueObjectsNull_ReturnsTrue()
        {
            FakeDomainValueObject left = null;
            FakeDomainValueObject right = null;

            var result = left == right;

            Assert.True(result);
        }

        [Fact]
        public void EqualityOverloadedOperator_LeftValueObjectNull_ReturnsFalse()
        {
            FakeDomainValueObject left = null;
            FakeDomainValueObject right = new FakeDomainValueObject();

            var result = left == right;

            Assert.False(result);
        }

        [Fact]
        public void EqualityOverloadedOperator_RightValueObjectNull_ReturnsFalse()
        {
            FakeDomainValueObject left = new FakeDomainValueObject();
            FakeDomainValueObject right = null;

            var result = left == right;

            Assert.False(result);
        }

        [Fact]
        public void EqualityOverloadedOperator_EntitiesWithSamePropertyValues_ReturnsTrue()
        {
            FakeDomainValueObject left = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };
            FakeDomainValueObject right = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };

            var result = left == right;

            Assert.True(result);
        }

        [Fact]
        public void EqualityOverloadedOperator_EntitiesWithDifferentPropertyValues_ReturnsFalse()
        {
            FakeDomainValueObject left = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };
            FakeDomainValueObject right = new FakeDomainValueObject() { PropertyOne = "Test", };

            var result = left == right;

            Assert.False(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_ValueObjectsSameInstance_ReturnsFalse()
        {
            FakeDomainValueObject left = new FakeDomainValueObject();
            FakeDomainValueObject right = left;

            var result = left != right;

            Assert.False(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_BothValueObjectsNull_ReturnsFalse()
        {
            FakeDomainValueObject left = null;
            FakeDomainValueObject right = null;

            var result = left != right;

            Assert.False(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_LeftValueObjectNull_ReturnsTrue()
        {
            FakeDomainValueObject left = null;
            FakeDomainValueObject right = new FakeDomainValueObject();

            var result = left != right;

            Assert.True(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_RightValueObjectNull_ReturnsTrue()
        {
            FakeDomainValueObject left = new FakeDomainValueObject();
            FakeDomainValueObject right = null;

            var result = left != right;

            Assert.True(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_EntitiesWithSamePropertyValues_ReturnsFalse()
        {
            FakeDomainValueObject left = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };
            FakeDomainValueObject right = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };

            var result = left != right;

            Assert.False(result);
        }

        [Fact]
        public void InequalityOverloadedOperator_EntitiesWithDifferentPropertyValues_ReturnsTrue()
        {
            FakeDomainValueObject left = new FakeDomainValueObject() { PropertyOne = "Test", PropertyTwo = 10 };
            FakeDomainValueObject right = new FakeDomainValueObject() { PropertyOne = "Test", };

            var result = left != right;

            Assert.True(result);
        }

        private class FakeDomainValueObject : DomainValueObject<FakeDomainValueObject>
        {
            public string PropertyOne { get; set; }

            public int PropertyTwo { get; set; }
        }
    }
}
