﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects.Tests
{
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class CardDetailsTests
    {
        private const string number = "4012 8888 8888 1881";
        private const string holderName = "Will Smith";
        private const int expirationMonth = 12;
        private const int expirationYear = 2020;
        private const string securityCode = "123";

        [Fact]
        public void Constructor_ValidCardDetails_ReturnsFilledCardDetails()
        {
            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);

            // Assert
            Assert.NotNull(cardDetails);
            Assert.Equal(number, cardDetails.Number);
            Assert.Equal(holderName, cardDetails.HolderName);
            Assert.Equal(expirationMonth, cardDetails.ExpirationMonth);
            Assert.Equal(expirationYear, cardDetails.ExpirationYear);
            Assert.Equal(securityCode, cardDetails.SecurityCode);
        }

        [Theory]
        [InlineData("4485735738234191", "************4191")]
        [InlineData("4716 5127 3000 9058", "**** **** **** 9058")]
        [InlineData("4539032534337486123", "***************6123")]
        public void ObfucateNumber_ValidCardDetails_SetsNumberPropertyAsObfuscated(string number, string expectedNumber)
        {
            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            cardDetails.ObfucateNumber();

            // Assert
            Assert.Equal(expectedNumber, cardDetails.Number);
        }

        [Theory]
        [InlineData("123", "***")]
        [InlineData("1234", "****")]
        public void ObfucateSecurityCode_ValidCardDetails_SetsSecurityCodePropertyAsObfuscated(string securityCode, string expectedSecurityCode)
        {
            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            cardDetails.ObfucateSecurityCode();

            // Assert
            Assert.Equal(expectedSecurityCode, cardDetails.SecurityCode);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData("        ")]
        public void Validate_NullOrEmptyNumber_ThrowsInvalidDomainValueObjectException(string number)
        {
            // Arrange
            var expectedMessage = "Card number cannot be null or empty";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("A4485735738234191A")]
        [InlineData("4485-7357-3823-4191")]
        public void Validate_InvalidFormatNumber_ThrowsInvalidDomainValueObjectException(string number)
        {
            // Arrange
            var expectedMessage = "Card number format is not valid [numbers and white spaces are accepted]";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("6011926506293654")]
        [InlineData("3543388785422153")]
        [InlineData("6762480946571023")]
        [InlineData("5423844237878103")]
        [InlineData("5343380757009263")]
        [InlineData("4485735738234192")]
        [InlineData("4716512730009059")]
        public void Validate_InvalidCardNumber_ReturnsValidCardDetails(string number)
        {
            // Arrange
            var expectedMessage = "Card number is not valid";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("6011926506293653")]
        [InlineData("3543388785422152")]
        [InlineData("6762480946571022")]
        [InlineData("5423844237878102")]
        [InlineData("5343380757009262")]
        [InlineData("4485735738234191")]
        [InlineData("4716512730009058")]
        public void Validate_ValidCardNumber_ReturnsValidCardDetails(string number)
        {
            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            cardDetails.Validate();

            // Assert
            Assert.NotNull(cardDetails);
            Assert.Equal(number, cardDetails.Number);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(13)]
        public void Validate_InvalidExpirationMonth_ThrowsInvalidDomainValueObjectException(int expirationMonth)
        {
            // Arrange
            var expectedMessage = "Card expiration month is not valid [1-12]";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData(1899)]
        [InlineData(10000)]
        public void Validate_InvalidExpirationYear_ThrowsInvalidDomainValueObjectException(int expirationYear)
        {
            // Arrange
            var expectedMessage = "Card expiration year is not valid [1900-9999]";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData(10, 2019)]
        [InlineData(8, 2020)]
        public void Validate_ExpiradCard_ThrowsInvalidDomainValueObjectException(int expirationMonth, int expirationYear)
        {
            // Arrange
            var expectedMessage = "Card validation is expired";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void Validate_NullOrEmptySecurityCode_ThrowsInvalidDomainValueObjectException(string securityCode)
        {
            // Arrange
            var expectedMessage = "Card security code cannot be null or empty";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData("123A")]
        [InlineData("ABCD")]
        [InlineData("@123")]
        [InlineData(" ")]
        [InlineData("        ")]
        public void Validate_InvalidFormatSecurityCode_ThrowsInvalidDomainValueObjectException(string securityCode)
        {
            // Arrange
            var expectedMessage = "Card security code must be a numric";

            // Act
            var cardDetails = new CardDetails(number, holderName, expirationMonth, expirationYear, securityCode);
            var exception = Assert.Throws<InvalidDomainValueObjectException>(() => cardDetails.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }
    }
}
