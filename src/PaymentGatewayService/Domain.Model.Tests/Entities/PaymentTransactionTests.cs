﻿namespace Checkout.PaymentGatewayService.Domain.Model.Entities.Tests
{
    using Checkout.PaymentGatewayService.Domain.Model.Enums;
    using Checkout.PaymentGatewayService.Domain.Model.ValueObjects;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;
    using Xunit;

    [Trait("Category", "Domain.Model")]
    public class PaymentTransactionTests
    {
        private const string merchantId = "123456";
        private const decimal amount = 10.25m;

        private readonly CardDetails _cardDetails;
        private readonly ThreeLetterCurrencyCode _currencyCode;

        public PaymentTransactionTests()
        {
            _cardDetails = new CardDetails("4012 8888 8888 1881", "Will Smith", 12, 2020, "123");
            _currencyCode = new ThreeLetterCurrencyCode("EUR");
        }

        [Fact]
        public void Constructor_ValidPaymentTransaction_NewPaymentTransactionWithFilledProperties()
        {
            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);

            // Assert
            Assert.NotNull(paymentTransaction);
            Assert.True(paymentTransaction.MerchantId.IsNullOrEmpty());
            Assert.Equal(_cardDetails, paymentTransaction.CardDetails);
            Assert.Equal(_currencyCode, paymentTransaction.CurrencyCode);
            Assert.Equal(amount, paymentTransaction.Amount);
            Assert.Equal(PaymentStatus.Pending, paymentTransaction.Status);
            Assert.True(paymentTransaction.Message.IsNullOrEmpty());
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void SetMerchantId_NullOrEmptyMerchantId_ThrowsInvalidDomainEntityException(string merchantId)
        {
            // Arrange
            var expectedMessage = "Merchant id cannot be null or empty";

            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => paymentTransaction.SetMerchantId(merchantId));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void SetMerchantId_ValidMerchantId_UpdatedMerchantIdProperty()
        {
            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            paymentTransaction.SetMerchantId(merchantId);

            // Assert
            Assert.NotNull(paymentTransaction);
            Assert.Equal(merchantId, paymentTransaction.MerchantId);
        }

        [Fact]
        public void AcceptPayment_AcceptPaymentTransaction_UpdatedStatusAndUpdatedOnProperties()
        {
            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            paymentTransaction.AcceptPayment();

            // Assert
            Assert.NotNull(paymentTransaction);
            Assert.Equal(PaymentStatus.Success, paymentTransaction.Status);
            Assert.NotNull(paymentTransaction.UpdatedOn);
        }

        [Fact]
        public void RejectPayment_InsufficientFundsPaymentTransaction_UpdatedStatusAndMessageAndUpdatedOnProperties()
        {
            // Arrange
            var rejectMessage = "Insufficient Funds";

            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            paymentTransaction.RejectPayment(rejectMessage);

            // Assert
            Assert.NotNull(paymentTransaction);
            Assert.Equal(PaymentStatus.Rejected, paymentTransaction.Status);
            Assert.Equal(rejectMessage, paymentTransaction.Message);
            Assert.NotNull(paymentTransaction.UpdatedOn);
        }

        [Fact]
        public void Validate_NullCardDetails_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var expectedMessage = "Card details cannot be null";

            // Act
            var paymentTransaction = new PaymentTransaction(null, _currencyCode, amount);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => paymentTransaction.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void Validate_NullCardCurrencyCode_ThrowsInvalidDomainEntityException()
        {
            // Arrange
            var expectedMessage = "Currency code cannot be null";

            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, null, amount);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => paymentTransaction.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Theory]
        [InlineData(-1.0)]
        [InlineData(0)]
        public void Validate_NegativeOrZeroAmount_ThrowsInvalidDomainEntityException(decimal amount)
        {
            // Arrange
            var expectedMessage = "Amount must be greater than zero";

            // Act
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            var exception = Assert.Throws<InvalidDomainEntityException>(() => paymentTransaction.Validate());

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }
    }
}
