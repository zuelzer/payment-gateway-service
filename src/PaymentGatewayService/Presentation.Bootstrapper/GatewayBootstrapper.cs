﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Implementations;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Implementations.Mock;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class GatewayBootstrapper
    {
        public static void RegisterServices(IServiceCollection services, BankDependecyConfiguration bankDependecyConfiguration)
        {
            if (bankDependecyConfiguration.Mocked)
            {
                services.AddScoped<IBankTransactionRepository, BankTransactionMockGateway>();
            }
            else
            {
                services.AddScoped<IBankTransactionRepository, BankTransactionGateway>();
            }
        }
    }
}
