﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using AutoMapper;
    using Checkout.PaymentGatewayService.Domain.Services.Implementations;
    using Checkout.PaymentGatewayService.Domain.Services.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    
    public class DomainBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IPaymentTransactionService, PaymentTransactionService>();
        }
    }
}
