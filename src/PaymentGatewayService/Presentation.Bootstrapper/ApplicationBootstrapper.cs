﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using Checkout.PaymentGatewayService.Application.Services.Implementations;
    using Checkout.PaymentGatewayService.Application.Services.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public class ApplicationBootstrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IPaymentTransactionAppService, PaymentTransactionAppService>();
        }
    }
}
