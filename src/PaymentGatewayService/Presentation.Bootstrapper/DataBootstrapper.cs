﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using System;
    using Checkout.PaymentGatewayService.Data.Repository.Context;
    using Checkout.PaymentGatewayService.Data.Repository.Implementations;
    using Checkout.PaymentGatewayService.Domain.Core.Repositories;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class DataBootstrapper
    {
        public static void RegisterServices(IServiceCollection services, EntityFrameworkConfiguration entityFrameworkConfiguration)
        {
            services.AddScoped<IPaymentTransactionRepository, PaymentTransactionRepository>();

            services.AddDbContext<SqlServerContext>();
            services.AddTransient<Func<SqlServerContext>>((provider) => new Func<SqlServerContext>(() => new SqlServerContext(entityFrameworkConfiguration)));
        }
    }
}
