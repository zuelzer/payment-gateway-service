﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class AppSettingsBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            BankDependecyConfiguration bankDependecyConfiguration,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddSingleton<BankDependecyConfiguration>(bankDependecyConfiguration);
            services.AddSingleton<EntityFrameworkConfiguration>(entityFrameworkConfiguration);
            services.AddSingleton<HealthCheckConfiguration>(healthCheckConfiguration);
            services.AddSingleton<LoggingConfiguration>(loggingConfiguration);
            services.AddSingleton<SwaggerConfiguration>(swaggerConfiguration);
        }
    }
}
