﻿namespace Checkout.PaymentGatewayService.Presentation.Bootstrapper
{
    using AutoMapper;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class NativeInjectorBootstrapper
    {
        public static void RegisterServices(IServiceCollection services,
            BankDependecyConfiguration bankDependecyConfiguration,
            EntityFrameworkConfiguration entityFrameworkConfiguration,
            HealthCheckConfiguration healthCheckConfiguration,
            LoggingConfiguration loggingConfiguration,
            SwaggerConfiguration swaggerConfiguration)
        {
            services.AddScoped(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            AppSettingsBootstrapper.RegisterServices(services, bankDependecyConfiguration, entityFrameworkConfiguration, healthCheckConfiguration, loggingConfiguration, swaggerConfiguration);
            ApplicationBootstrapper.RegisterServices(services);
            DomainBootstrapper.RegisterServices(services);
            DataBootstrapper.RegisterServices(services, entityFrameworkConfiguration);
            GatewayBootstrapper.RegisterServices(services, bankDependecyConfiguration);
        }
    }
}
