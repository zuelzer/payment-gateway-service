﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces
{
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities;

    public interface IBankTransactionRepository
    {
        Task<TransactionResponse> PerformTransaction(TransactionRequest transactionRequest);
    }
}
