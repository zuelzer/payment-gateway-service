﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Implementations
{
    using System;
    using System.Net.Http;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Http;

    public class BankBaseGateway
    {
        private const string userAgent = "payment-gateway-service/v1";

        protected readonly Uri _baseAddress;
        protected readonly BankDependecyConfiguration _bankDependecyConfiguration;

        protected IHttpConnection HttpConnection { get; private set; }

        protected HttpHeader[] HttpHeaders { get; private set; }

        protected BankBaseGateway(BankDependecyConfiguration bankDependecyConfiguration)
        {
            this._bankDependecyConfiguration = bankDependecyConfiguration;
            this._baseAddress = new Uri(this._bankDependecyConfiguration.BaseEndPointUrl);

            this.HttpHeaders = new HttpHeader[] {
                new HttpHeader("SecretToken", this._bankDependecyConfiguration.SecretToken)
            };

            var httpClient = new HttpClient
            {
                BaseAddress = _baseAddress
            };

            this.HttpConnection = new HttpClientConnection(httpClient, userAgent);
        }
    }
}
