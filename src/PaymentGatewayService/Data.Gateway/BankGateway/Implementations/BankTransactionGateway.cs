﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Enum;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration;
    using Microsoft.Extensions.Logging;

    public class BankTransactionGateway : BankBaseGateway, IBankTransactionRepository
    {
        private readonly ILogger _logger;

        public BankTransactionGateway(ILoggerFactory loggerFactory, BankDependecyConfiguration bankDependecyConfiguration)
            : base(bankDependecyConfiguration)
        {
            this._logger = loggerFactory.CreateLogger<BankTransactionGateway>();
        }

        public async Task<TransactionResponse> PerformTransaction(TransactionRequest transactionRequest)
        {
            var performTransactionUrl = new Uri(base._baseAddress, $"{base._bankDependecyConfiguration.CustomerKey}/Transactions");

            try
            {
                return await HttpConnection.PostAsync<TransactionRequest, TransactionResponse>(performTransactionUrl, transactionRequest, base.HttpHeaders).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to perform a transaction to the acquiring bank: {url}", performTransactionUrl.AbsolutePath);

                return new TransactionResponse
                {
                    Status = TransactionStatus.Unknown,
                    Message = "Unabled to perform a transaction to the acquiring bank"
                };
            }
        }
    }
}
