﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Implementations.Mock
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Enum;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces;

    public class BankTransactionMockGateway : IBankTransactionRepository
    {
        private readonly Random _random;
        private readonly TransactionResponse[] _dataMock;

        public BankTransactionMockGateway()
        {
            this._random = new Random();

            this._dataMock = new TransactionResponse[]
            {
                new TransactionResponse { Status = TransactionStatus.Success },
                new TransactionResponse { Status = TransactionStatus.Rejected, Message = "Insufficient Funds" },
                new TransactionResponse { Status = TransactionStatus.Rejected, Message = "Your account is closed" },
                new TransactionResponse { Status = TransactionStatus.Rejected, Message = "Your payment is past due" },
                new TransactionResponse { Status = TransactionStatus.Rejected, Message = "Your purchase was flagged as fraud" },
            };
        }

        public async Task<TransactionResponse> PerformTransaction(TransactionRequest transactionRequest)
        {
            TransactionResponse transactionResponse = null;

            await Task.Run(() =>
            {
                var randomIndex = this._random.Next(0, this._dataMock.Length - 1);

                transactionResponse = this._dataMock[randomIndex];
            });

            return transactionResponse;
        }
    }
}
