﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities
{
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Enum;

    public class TransactionResponse
    {
        public TransactionStatus Status { get; set; }

        public string Message { get; set; }
    }
}
