﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities
{
    public class TransactionRequest
    {
        public string CardNumber { get; set; }

        public string CardholderName { get; set; }

        public int ExpirationMonth { get; set; }

        public int ExpirationYear { get; set; }

        public string CardSecurityCode { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }
    }
}
