﻿namespace Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Enum
{
    public enum TransactionStatus
    {
        Unknown = 0,
        Success = 1,
        Rejected = 2
    }
}
