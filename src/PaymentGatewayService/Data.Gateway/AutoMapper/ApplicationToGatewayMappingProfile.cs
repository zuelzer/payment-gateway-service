﻿namespace Checkout.PaymentGatewayService.Data.Gateway.AutoMapper
{
    using Checkout.PaymentGatewayService.Application.DTO.Request;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities;
    using global::AutoMapper;

    public class ApplicationToGatewayMappingProfile : Profile
    {
        public ApplicationToGatewayMappingProfile()
        {
            CreateMap<PaymentTransaction, TransactionRequest>()
                .ForMember(s => s.CardNumber, d => d.MapFrom(t => t.CardDetails.Number))
                .ForMember(s => s.CardholderName, d => d.MapFrom(t => t.CardDetails.HolderName))
                .ForMember(s => s.ExpirationMonth, d => d.MapFrom(t => t.CardDetails.ExpirationMonth))
                .ForMember(s => s.ExpirationYear, d => d.MapFrom(t => t.CardDetails.ExpirationYear))
                .ForMember(s => s.CardSecurityCode, d => d.MapFrom(t => t.CardDetails.SecurityCode))
                .ForMember(s => s.Currency, d => d.MapFrom(t => t.CurrencyCode));
        }
    }
}
