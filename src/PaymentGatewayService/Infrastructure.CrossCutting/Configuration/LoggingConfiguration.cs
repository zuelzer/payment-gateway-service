﻿namespace Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration
{
    public class LoggingConfiguration
    {
        public string ApplicationInsightUrl { get; set; }
    }
}
