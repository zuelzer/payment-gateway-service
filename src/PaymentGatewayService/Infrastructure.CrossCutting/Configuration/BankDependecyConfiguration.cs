﻿namespace Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Configuration
{
    using System;

    public class BankDependecyConfiguration
    {
        public string BaseEndPointUrl { get; set; }

        public Guid CustomerKey { get; set; }

        public string SecretToken { get; set; }

        public bool Mocked { get; set; }
    }
}
