﻿namespace Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Serializer
{
    public interface IStringSerializer
    {
        string SerializeToString(object item);
    }
}
