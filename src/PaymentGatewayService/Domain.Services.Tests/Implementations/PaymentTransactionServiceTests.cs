﻿namespace Checkout.PaymentGatewayService.Domain.Services.Implementations.Tests
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Domain.Core.Repositories;
    using Checkout.PaymentGatewayService.Domain.Model.Entities;
    using Checkout.PaymentGatewayService.Domain.Model.Enums;
    using Checkout.PaymentGatewayService.Domain.Model.ValueObjects;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Moq;
    using Xunit;

    [Trait("Category", "Domain.Services")]
    public class PaymentTransactionServiceTests
    {
        private const string merchantId = "123456";
        private const decimal amount = 10.25m;

        private readonly Mock<IPaymentTransactionRepository> _paymentTransactionRepositoryMock;

        private readonly CardDetails _cardDetails;
        private readonly ThreeLetterCurrencyCode _currencyCode;

        public PaymentTransactionServiceTests()
        {
            this._paymentTransactionRepositoryMock = new Mock<IPaymentTransactionRepository>();

            _cardDetails = new CardDetails("4012 8888 8888 1881", "Will Smith", 12, 2020, "123");
            _currencyCode = new ThreeLetterCurrencyCode("EUR");
        }

        [Fact]
        public async Task GetPaymentTransactionAsync_NotFoundPaymentTransaction_ThrowsResourceNotFoundException()
        {
            // Arrange
            var paymentTransactionId = Guid.NewGuid();
            var expectedMessage = "Payment transaction not found.";

            // Act
            var service = this.CreatePaymentTransactionService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.GetPaymentTransactionAsync(merchantId, paymentTransactionId));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async Task GetPaymentTransactionAsync_ExistsPaymentTransaction_ReturnsFilledPaymentTransaction()
        {
            // Arrange
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            paymentTransaction.SetMerchantId(merchantId);

            this._paymentTransactionRepositoryMock.Setup(m => m.GetAsync(It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(paymentTransaction);

            // Act
            var service = this.CreatePaymentTransactionService();
            var result = await service.GetPaymentTransactionAsync(paymentTransaction.MerchantId, paymentTransaction.Id);

            // Assert
            Assert.Equal(paymentTransaction.Id, result.Id);
            Assert.Equal(paymentTransaction.MerchantId, result.MerchantId);
            Assert.Equal(paymentTransaction.CardDetails.Number, result.CardDetails.Number);
            Assert.Equal(paymentTransaction.CardDetails.HolderName, result.CardDetails.HolderName);
            Assert.Equal(paymentTransaction.CardDetails.ExpirationMonth, result.CardDetails.ExpirationMonth);
            Assert.Equal(paymentTransaction.CardDetails.ExpirationYear, result.CardDetails.ExpirationYear);
            Assert.Equal(paymentTransaction.CardDetails.SecurityCode, result.CardDetails.SecurityCode);
            Assert.Equal(paymentTransaction.CurrencyCode.Value, result.CurrencyCode.Value);
            Assert.Equal(paymentTransaction.Amount, result.Amount);
            Assert.Equal(paymentTransaction.Status, result.Status);
            Assert.Equal(paymentTransaction.Message, result.Message);
            Assert.Equal(paymentTransaction.CreatedOn, result.CreatedOn);
            Assert.Equal(paymentTransaction.UpdatedOn, result.UpdatedOn);
        }

        [Fact]
        public async Task CreatePaymentTransactionAsync_ValidPaymentTransaction_CallsInsertAsyncOnce()
        {
            // Arrange
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);

            // Act
            var service = this.CreatePaymentTransactionService();
            await service.CreatePaymentTransactionAsync(merchantId, paymentTransaction);

            // Assert
            this._paymentTransactionRepositoryMock.Verify(m => m.InsertAsync(It.IsAny<PaymentTransaction>()), Times.Once);
        }

        [Fact]
        public async Task AcceptPaymentTransactionAsync_NotFoundPaymentTransaction_ThrowsResourceNotFoundException()
        {
            // Arrange
            var paymentTransactionId = Guid.NewGuid();
            var expectedMessage = "Payment transaction not found.";

            // Act
            var service = this.CreatePaymentTransactionService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.AcceptPaymentTransactionAsync(merchantId, paymentTransactionId));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async Task AcceptPaymentTransactionAsync_ExistsPaymentTransaction_UpdatesStatusPropertyAndCallsUpdateAsyncOnce()
        {
            // Arrange
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);

            this._paymentTransactionRepositoryMock.Setup(m => m.GetAsync(It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(paymentTransaction);

            // Act
            var service = this.CreatePaymentTransactionService();
            await service.AcceptPaymentTransactionAsync(paymentTransaction.MerchantId, paymentTransaction.Id);

            // Assert
            Assert.Equal(PaymentStatus.Success, paymentTransaction.Status);
            Assert.NotNull(paymentTransaction.UpdatedOn);
            this._paymentTransactionRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<PaymentTransaction>()), Times.Once);
        }

        [Fact]
        public async Task RejectPaymentTransactionAsync_NotFoundPaymentTransaction_ThrowsResourceNotFoundException()
        {
            // Arrange
            var paymentTransactionId = Guid.NewGuid();
            var rejectMessage = "Insufficient Funds";
            var expectedMessage = "Payment transaction not found.";

            // Act
            var service = this.CreatePaymentTransactionService();
            var exception = await Assert.ThrowsAsync<ResourceNotFoundException>(() => service.RejectPaymentTransactionAsync(merchantId, paymentTransactionId, rejectMessage));

            // Assert
            Assert.NotNull(exception);
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public async Task RejectPaymentTransactionAsync_ExistsPaymentTransaction_UpdatesStatusAndMessagePropertiesAndCallsUpdateAsyncOnce()
        {
            // Arrange
            var paymentTransaction = new PaymentTransaction(_cardDetails, _currencyCode, amount);
            var rejectMessage = "Insufficient Funds";

            this._paymentTransactionRepositoryMock.Setup(m => m.GetAsync(It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(paymentTransaction);

            // Act
            var service = this.CreatePaymentTransactionService();
            await service.RejectPaymentTransactionAsync(paymentTransaction.MerchantId, paymentTransaction.Id, rejectMessage);

            // Assert
            Assert.Equal(PaymentStatus.Rejected, paymentTransaction.Status);
            Assert.Equal(rejectMessage, paymentTransaction.Message);
            Assert.NotNull(paymentTransaction.UpdatedOn);
            this._paymentTransactionRepositoryMock.Verify(m => m.UpdateAsync(It.IsAny<PaymentTransaction>()), Times.Once);
        }

        private PaymentTransactionService CreatePaymentTransactionService()
        {
            return new PaymentTransactionService(this._paymentTransactionRepositoryMock.Object);
        }
    }
}
