﻿namespace Checkout.PaymentGatewayService.Application.Services.Implementations.Tests
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Application.DTO.Request;
    using Checkout.PaymentGatewayService.Application.Services.AutoMapper;
    using Checkout.PaymentGatewayService.Application.Services.Interfaces;
    using Checkout.PaymentGatewayService.Data.Gateway.AutoMapper;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces;
    using Checkout.PaymentGatewayService.Domain.Services.Interfaces;
    using global::AutoMapper;
    using Microsoft.Extensions.Logging;
    using Moq;
    using Xunit;

    [Trait("Category", "Application.Services")]
    public class PaymentTransactionAppServiceTests
    {
        private readonly Mock<ILoggerFactory> _loggerFactoryMock;
        private readonly Mock<IPaymentTransactionService> _paymentTransactionServiceMock;
        private readonly Mock<IBankTransactionRepository> _bankTransactionRepositoryMock;

        public PaymentTransactionAppServiceTests()
        {
            _loggerFactoryMock = new Mock<ILoggerFactory>();
            _paymentTransactionServiceMock = new Mock<IPaymentTransactionService>();
            _bankTransactionRepositoryMock = new Mock<IBankTransactionRepository>();
        }

        [Fact]
        public async Task GetPaymentTransactionAsync_ExistsPaymentTransaction_ReturnsFilledPaymentTransaction()
        {
            // Arrange
            var merchantId = "123456";
            var cardDetails = new Domain.Model.ValueObjects.CardDetails("4012 8888 8888 1881", "Will Smith", 12, 2020, "123");
            var currencyCode = new Domain.Model.ValueObjects.ThreeLetterCurrencyCode("EUR");
            var amount = 10.25m;
            var paymentTransaction = new Domain.Model.Entities.PaymentTransaction(cardDetails, currencyCode, amount);
            paymentTransaction.SetMerchantId(merchantId);
            paymentTransaction.AcceptPayment();

            this._paymentTransactionServiceMock.Setup(m => m.GetPaymentTransactionAsync(It.IsAny<string>(), It.IsAny<Guid>())).ReturnsAsync(paymentTransaction);

            // Act
            var service = this.CreatePaymentTransactionAppService();
            var filledPaymentTransaction = await service.GetPaymentTransactionAsync(merchantId, paymentTransaction.Id);

            // Assert
            Assert.NotNull(filledPaymentTransaction);
            Assert.NotNull(filledPaymentTransaction.CardDetails);
            Assert.NotNull(filledPaymentTransaction.CurrencyCode);
            Assert.Equal(paymentTransaction.Status.ToString(), filledPaymentTransaction.Status);
            Assert.Equal(paymentTransaction.Message, filledPaymentTransaction.Message);
            Assert.Equal(paymentTransaction.UpdatedOn, filledPaymentTransaction.ProcessedOn);
            Assert.Equal(cardDetails.Number, filledPaymentTransaction.CardDetails.ObfuscatedNumber);
            Assert.Equal(cardDetails.HolderName, filledPaymentTransaction.CardDetails.HolderName);
            Assert.Equal(cardDetails.ExpirationMonth, filledPaymentTransaction.CardDetails.ExpirationMonth);
            Assert.Equal(cardDetails.ExpirationYear, filledPaymentTransaction.CardDetails.ExpirationYear);
            Assert.Equal(currencyCode.ToString(), filledPaymentTransaction.CurrencyCode);
            Assert.Equal(amount, filledPaymentTransaction.Amount);
        }

        [Fact]
        public async Task CreatePaymentTransactionAsync_ValidPaymentTransaction_ReturnsFilledPaymentTransaction()
        {
            // Arrange
            var merchantId = "123456";
            var amount = 10.25m;
            var paymentTransaction = new PaymentTransaction
            {
                CardDetails = new CardDetails
                {
                    Number = "4012 8888 8888 1881",
                    HolderName = "Will Smith",
                    ExpirationMonth = 12,
                    ExpirationYear = 2020,
                    SecurityCode = "123"
                },
                CurrencyCode = "EUR",
                Amount = amount
            };

            // Act
            var service = this.CreatePaymentTransactionAppService();
            var createdPaymentTransaction = await service.CreatePaymentTransactionAsync(merchantId, paymentTransaction);

            // Assert
            Assert.NotNull(createdPaymentTransaction);
            Assert.NotNull(createdPaymentTransaction.CardDetails);
            Assert.NotNull(createdPaymentTransaction.CurrencyCode);
            Assert.NotEqual(Guid.Empty, createdPaymentTransaction.Id);
            Assert.Equal(paymentTransaction.CardDetails.Number, createdPaymentTransaction.CardDetails.ObfuscatedNumber);
            Assert.Equal(paymentTransaction.CardDetails.HolderName, createdPaymentTransaction.CardDetails.HolderName);
            Assert.Equal(paymentTransaction.CardDetails.ExpirationMonth, createdPaymentTransaction.CardDetails.ExpirationMonth);
            Assert.Equal(paymentTransaction.CardDetails.ExpirationYear, createdPaymentTransaction.CardDetails.ExpirationYear);
            Assert.Equal(paymentTransaction.CurrencyCode, createdPaymentTransaction.CurrencyCode);
            Assert.Equal(paymentTransaction.Amount, createdPaymentTransaction.Amount);
            this._paymentTransactionServiceMock.Verify(m => m.CreatePaymentTransactionAsync(It.IsAny<string>(), It.IsAny<Domain.Model.Entities.PaymentTransaction>()), Times.Once);
        }

        private IPaymentTransactionAppService CreatePaymentTransactionAppService()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ApplicationToDomainMappingProfile());
                cfg.AddProfile(new DomainToApplicationMappingProfile());
                cfg.AddProfile(new ApplicationToGatewayMappingProfile());
            });

            var _mapper = mapperConfiguration.CreateMapper();

            return new PaymentTransactionAppService(_loggerFactoryMock.Object, _mapper, this._paymentTransactionServiceMock.Object, this._bankTransactionRepositoryMock.Object);
        }
    }
}
