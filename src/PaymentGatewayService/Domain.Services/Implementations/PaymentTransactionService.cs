﻿namespace Checkout.PaymentGatewayService.Domain.Services.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Domain.Core.Repositories;
    using Checkout.PaymentGatewayService.Domain.Model.Entities;
    using Checkout.PaymentGatewayService.Domain.Services.Interfaces;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;

    public class PaymentTransactionService : IPaymentTransactionService
    {
        private readonly IPaymentTransactionRepository _paymentTransactionRepository;

        public PaymentTransactionService(IPaymentTransactionRepository paymentTransactionRepository)
        {
            this._paymentTransactionRepository = paymentTransactionRepository;
        }

        public async Task<PaymentTransaction> GetPaymentTransactionAsync(string merchantId, Guid paymentTransactionId)
        {
            var paymentTransaction = await this._paymentTransactionRepository.GetAsync(merchantId, paymentTransactionId).ConfigureAwait(false);

            Guard.Against<ResourceNotFoundException>(paymentTransaction.IsNull(), "Payment transaction not found.");

            return paymentTransaction;
        }

        public async Task CreatePaymentTransactionAsync(string merchantId, PaymentTransaction paymentTransaction)
        {
            Guard.Against<InvalidDomainEntityException>(merchantId.IsNullOrEmpty(), "Merchant id cannot be null or empty");
            Guard.Against<ResourceNotFoundException>(paymentTransaction.IsNull(), "Payment transaction cannot be null.");

            var dataPaymentTransaction = await this._paymentTransactionRepository.GetAsync(merchantId, paymentTransaction.Id).ConfigureAwait(false);

            Guard.Against<ResourceAlreadyExistsException>(dataPaymentTransaction.IsNotNull(), "Payment transaction already exists.");

            paymentTransaction.SetMerchantId(merchantId);
            paymentTransaction.Validate();

            paymentTransaction.CardDetails.ObfucateNumber();
            paymentTransaction.CardDetails.ObfucateSecurityCode();

            await this._paymentTransactionRepository.InsertAsync(paymentTransaction).ConfigureAwait(false);
        }

        public async Task AcceptPaymentTransactionAsync(string merchantId, Guid paymentTransactionId)
        {
            var paymentTransaction = await this._paymentTransactionRepository.GetAsync(merchantId, paymentTransactionId).ConfigureAwait(false);

            Guard.Against<ResourceNotFoundException>(paymentTransaction.IsNull(), "Payment transaction not found.");

            paymentTransaction.AcceptPayment();

            await this._paymentTransactionRepository.UpdateAsync(paymentTransaction).ConfigureAwait(false);
        }

        public async Task RejectPaymentTransactionAsync(string merchantId, Guid paymentTransactionId, string message)
        {
            var paymentTransaction = await this._paymentTransactionRepository.GetAsync(merchantId, paymentTransactionId).ConfigureAwait(false);

            Guard.Against<ResourceNotFoundException>(paymentTransaction.IsNull(), "Payment transaction not found.");

            paymentTransaction.RejectPayment(message);

            await this._paymentTransactionRepository.UpdateAsync(paymentTransaction).ConfigureAwait(false);
        }
    }
}
