﻿namespace Checkout.PaymentGatewayService.Domain.Services.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Domain.Model.Entities;

    public interface IPaymentTransactionService
    {
        Task<PaymentTransaction> GetPaymentTransactionAsync(string merchantId, Guid paymentTransactionId);

        Task CreatePaymentTransactionAsync(string merchantId, PaymentTransaction paymentTransaction);

        Task AcceptPaymentTransactionAsync(string merchantId, Guid paymentTransactionId);

        Task RejectPaymentTransactionAsync(string merchantId, Guid paymentTransactionId, string message);
    }
}
