﻿namespace Checkout.PaymentGatewayService.Domain.Model.Enums
{
    public enum PaymentStatus
    {
        Pending = 0,
        Success = 1,
        Rejected = 2
    }
}
