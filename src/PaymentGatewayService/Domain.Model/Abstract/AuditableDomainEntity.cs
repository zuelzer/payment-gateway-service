﻿namespace Checkout.PaymentGatewayService.Domain.Model.Abstract
{
    using System;
    using Checkout.PaymentGatewayService.Domain.Model.Interfaces;

    public abstract class AuditableDomainEntity : DomainEntity, IAuditableEntity
    {
        protected AuditableDomainEntity()
        {
            this.CreatedOn = DateTime.UtcNow;
        }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }
    }
}
