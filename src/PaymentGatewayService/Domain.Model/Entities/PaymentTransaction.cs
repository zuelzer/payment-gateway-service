﻿namespace Checkout.PaymentGatewayService.Domain.Model.Entities
{
    using System;
    using Checkout.PaymentGatewayService.Domain.Model.Abstract;
    using Checkout.PaymentGatewayService.Domain.Model.Enums;
    using Checkout.PaymentGatewayService.Domain.Model.ValueObjects;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;

    public class PaymentTransaction : AuditableDomainEntity
    {
        protected PaymentTransaction() { }

        public PaymentTransaction(CardDetails cardDetails, ThreeLetterCurrencyCode currency, decimal amount)
        {
            this.CardDetails = cardDetails;
            this.CurrencyCode = currency;
            this.Amount = amount;
        }

        public string MerchantId { get; private set; }

        public CardDetails CardDetails { get; private set; }

        public ThreeLetterCurrencyCode CurrencyCode { get; private set; }

        public decimal Amount { get; private set; }

        public PaymentStatus Status { get; private set; }
        
        public string Message { get; private set; }

        public void SetMerchantId(string merchantId)
        {
            Guard.Against<InvalidDomainEntityException>(merchantId.IsNullOrEmpty(), "Merchant id cannot be null or empty");

            this.MerchantId = merchantId;
        }

        public void AcceptPayment()
        {
            this.Status = PaymentStatus.Success;
            base.UpdatedOn = DateTime.Now;
        }

        public void RejectPayment(string message)
        {
            this.Status = PaymentStatus.Rejected;
            this.Message = message;
            base.UpdatedOn = DateTime.Now;
        }

        public void Validate()
        {
            Guard.Against<InvalidDomainEntityException>(this.CardDetails.IsNull(), "Card details cannot be null");
            Guard.Against<InvalidDomainEntityException>(this.CurrencyCode.IsNull(), "Currency code cannot be null");
            Guard.Against<InvalidDomainEntityException>(this.Amount <= 0, "Amount must be greater than zero");

            CardDetails.Validate();
            CurrencyCode.Validate();
        }
    }
}
