﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects
{
    using System;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;
    using CreditCardValidator;

    public class CardDetails : DomainValueObject<CardDetails>
    {
        protected CardDetails() { }

        public CardDetails(string number, string holderName, int expirationMonth, int expirationYear, string securityCode)
        {
            this.Number = number;
            this.HolderName = holderName;
            this.ExpirationMonth = expirationMonth;
            this.ExpirationYear = expirationYear;
            this.SecurityCode = securityCode;
        }

        public string Number { get; private set; }

        public string HolderName { get; private set; }

        public int ExpirationMonth { get; private set; }

        public int ExpirationYear { get; private set; }

        public string SecurityCode { get; private set; }

        public void ObfucateNumber()
        {
            var obfucatedNumber = string.Empty;

            int count = 1;

            foreach (var item in this.Number)
            {
                if (item == (char)32 || count > this.Number.Length - 4)
                {
                    obfucatedNumber += item;
                }
                else
                {
                    obfucatedNumber += "*";
                }

                count++;
            }

            this.Number = obfucatedNumber;
        }

        public void ObfucateSecurityCode()
        {
            var obfucatedSecurityCode = string.Empty;

            foreach (var item in this.SecurityCode)
            {
                obfucatedSecurityCode += "*";
            }

            this.SecurityCode = obfucatedSecurityCode;
        }

        public void Validate()
        {
            Guard.Against<InvalidDomainValueObjectException>(this.Number.IsNullOrEmpty(), "Card number cannot be null or empty");
            Guard.Against<InvalidDomainValueObjectException>(this.Number.IsNullOrEmpty() || this.Number.IsNullOrWhiteSpace(), "Card number cannot be null or empty");
            Guard.Against<InvalidDomainValueObjectException>(!this.Number.IsNumberWithWhiteSpace(), "Card number format is not valid [numbers and white spaces are accepted]");

            CreditCardDetector detector = new CreditCardDetector(this.Number);

            Guard.Against<InvalidDomainValueObjectException>(!detector.IsValid(), "Card number is not valid");
            Guard.Against<InvalidDomainValueObjectException>(!this.IsValidMonth(this.ExpirationMonth), "Card expiration month is not valid [1-12]");
            Guard.Against<InvalidDomainValueObjectException>(!this.IsValidYear(this.ExpirationYear), "Card expiration year is not valid [1900-9999]");
            Guard.Against<InvalidDomainValueObjectException>(this.IsExpired(this.ExpirationMonth, this.ExpirationYear), "Card validation is expired");
            Guard.Against<InvalidDomainValueObjectException>(this.SecurityCode.IsNullOrEmpty(), "Card security code cannot be null or empty");
            Guard.Against<InvalidDomainValueObjectException>(!this.SecurityCode.IsNumber(), "Card security code must be a numric");
        }

        private bool IsExpired(int month, int year)
        {
            var expirationDate = new DateTime(year, month, 1, 23, 59, 59, 999).AddMonths(1).AddDays(-1);

            return DateTime.Now > expirationDate;
        }

        private bool IsValidMonth(int month)
        {
            return month >= 1 && month <= 12;
        }

        private bool IsValidYear(int year)
        {
            return year >= 1900 && year <= 9999;
        }
    }
}
