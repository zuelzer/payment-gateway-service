﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects
{
    using System.Reflection;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;

    public abstract class DomainValueObject<T> where T : DomainValueObject<T>
    {
        protected DomainValueObject()
        {
        }

        public static bool operator !=(DomainValueObject<T> a, DomainValueObject<T> b)
        {
            return !(a == b);
        }

        public static bool operator ==(DomainValueObject<T> left, DomainValueObject<T> right)
        {
            if (object.ReferenceEquals(left, right))
            {
                return true;
            }

            if (((object)left).IsNull() || ((object)right).IsNull())
            {
                return false;
            }

            foreach (PropertyInfo propertyInfo in left.GetType().GetProperties())
            {
                if (propertyInfo.CanRead)
                {
                    object leftValue = propertyInfo.FastGetValue(left);
                    object rightValue = propertyInfo.FastGetValue(right);

                    if (!object.Equals(leftValue, rightValue))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            return this == (obj as DomainValueObject<T>);
        }

        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    }
}
