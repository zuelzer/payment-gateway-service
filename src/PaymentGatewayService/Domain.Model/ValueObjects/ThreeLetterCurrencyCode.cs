﻿namespace Checkout.PaymentGatewayService.Domain.Model.ValueObjects
{
    using System.Linq;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Exceptions;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;

    public class ThreeLetterCurrencyCode : DomainValueObject<ThreeLetterCurrencyCode>
    {
        private static readonly string[] KnownCurrencyCodes = new string[] {"CAD", "CHF", "EUR", "GBP", "USD" };

        public ThreeLetterCurrencyCode(string value)
        {
            this.Value = value?.ToUpper();
        }

        public string Value { get; private set; }

        public override string ToString()
        {
            return this.Value;
        }

        public void Validate()
        {
            Guard.Against<InvalidDomainValueObjectException>(this.Value.IsNullOrEmpty(), "Currency code cannot be null or empty");
            Guard.Against<InvalidDomainValueObjectException>(this.Value.Length != 3, "Currency code should have three letters");
            Guard.Against<InvalidDomainValueObjectException>(!IsCurrencyCodeValid(this.Value), $"Currency code is not valid [Accepted currency codes: { string.Join(", ", KnownCurrencyCodes) }]");
        }

        private static bool IsCurrencyCodeValid(string countryCode)
        {
            return KnownCurrencyCodes.Contains(countryCode);
        }
    }
}
