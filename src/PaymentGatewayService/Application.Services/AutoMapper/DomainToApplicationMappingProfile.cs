﻿namespace Checkout.PaymentGatewayService.Application.Services.AutoMapper
{
    using Checkout.PaymentGatewayService.Application.DTO.Response;
    using global::AutoMapper;

    public class DomainToApplicationMappingProfile : Profile
    {
        public DomainToApplicationMappingProfile()
        {
            CreateMap<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>()
                .ForMember(s => s.Status, d => d.MapFrom(t => t.Status.ToString()))
                .ForMember(s => s.ProcessedOn, d => d.MapFrom(t => t.UpdatedOn))
                .ForMember(s => s.CardDetails, d => d.MapFrom(t => new CardDetails { ObfuscatedNumber = t.CardDetails.Number, HolderName = t.CardDetails.HolderName, ExpirationMonth = t.CardDetails.ExpirationMonth, ExpirationYear = t.CardDetails.ExpirationYear }))
                .ForMember(s => s.CurrencyCode, d => d.MapFrom(t => t.CurrencyCode.ToString()));
        }
    }
}
