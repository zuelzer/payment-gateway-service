﻿namespace Checkout.PaymentGatewayService.Application.Services.AutoMapper
{
    using Checkout.PaymentGatewayService.Application.DTO.Request;
    using Checkout.PaymentGatewayService.Domain.Model.ValueObjects;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;

    public class ApplicationToDomainMappingProfile : Profile
    {
        public ApplicationToDomainMappingProfile()
        {
            CreateMap<PaymentTransaction, Domain.Model.Entities.PaymentTransaction>()
                .ForMember(s => s.CardDetails, d => d.MapFrom(t => t.CardDetails.IsNull() ? null : new Domain.Model.ValueObjects.CardDetails(t.CardDetails.Number, t.CardDetails.HolderName, t.CardDetails.ExpirationMonth, t.CardDetails.ExpirationYear, t.CardDetails.SecurityCode)))
                .ForMember(s => s.CurrencyCode, d => d.MapFrom(t => new ThreeLetterCurrencyCode(t.CurrencyCode)));
        }
    }
}
