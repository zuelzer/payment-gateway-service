﻿namespace Checkout.PaymentGatewayService.Application.Services.Implementations
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Application.DTO.Response;
    using Checkout.PaymentGatewayService.Application.Services.Interfaces;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Entities;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Enum;
    using Checkout.PaymentGatewayService.Data.Gateway.BankGateway.Interfaces;
    using Checkout.PaymentGatewayService.Domain.Services.Interfaces;
    using Checkout.PaymentGatewayService.Infrastructure.CrossCutting.Extensions;
    using global::AutoMapper;
    using Microsoft.Extensions.Logging;

    public class PaymentTransactionAppService : IPaymentTransactionAppService
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IPaymentTransactionService _paymentTransactionService;
        private readonly IBankTransactionRepository _bankTransactionRepository;

        public PaymentTransactionAppService(ILoggerFactory loggerFactory, IMapper mapper, IPaymentTransactionService paymentTransactionService, IBankTransactionRepository bankTransactionRepository)
        {
            this._logger = loggerFactory.CreateLogger<PaymentTransactionAppService>();
            this._mapper = mapper;
            this._paymentTransactionService = paymentTransactionService;
            this._bankTransactionRepository = bankTransactionRepository;
        }

        public async Task<PaymentTransaction> GetPaymentTransactionAsync(string merchantId, Guid paymentTransactionId)
        {
            var domainPaymentTransaction = await this._paymentTransactionService.GetPaymentTransactionAsync(merchantId, paymentTransactionId).ConfigureAwait(false);

            return this._mapper.Map<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>(domainPaymentTransaction);
        }

        public async Task<PaymentTransaction> CreatePaymentTransactionAsync(string merchantId, DTO.Request.PaymentTransaction paymentTransaction)
        {
            var domainPaymentTransaction = this._mapper.Map<DTO.Request.PaymentTransaction, Domain.Model.Entities.PaymentTransaction>(paymentTransaction);
            var gatewayTransactionRequest = this._mapper.Map<DTO.Request.PaymentTransaction, TransactionRequest>(paymentTransaction);

            await this._paymentTransactionService.CreatePaymentTransactionAsync(merchantId, domainPaymentTransaction).ConfigureAwait(false);

            Task.Run(() => this.PerformTransaction(merchantId, domainPaymentTransaction.Id, gatewayTransactionRequest));

            return this._mapper.Map<Domain.Model.Entities.PaymentTransaction, PaymentTransaction>(domainPaymentTransaction);
        }

        private async Task PerformTransaction(string merchantId, Guid paymentTransactionId, TransactionRequest gatewayTransactionRequest)
        {
            _logger.LogInformation("Sending transaction to the acquiring bank - merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId}", merchantId, paymentTransactionId);

            var transactionResponse = await this._bankTransactionRepository.PerformTransaction(gatewayTransactionRequest).ConfigureAwait(false);

            if (transactionResponse.IsNotNull())
            {
                if (transactionResponse.Status == TransactionStatus.Success)
                {
                    _logger.LogInformation("Transaction approved by the acquiring bank - merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId}", merchantId, paymentTransactionId);

                    await this._paymentTransactionService.AcceptPaymentTransactionAsync(merchantId, paymentTransactionId).ConfigureAwait(false);
                }
                else if (transactionResponse.Status == TransactionStatus.Rejected)
                {
                    _logger.LogInformation("Transaction rejected by the acquiring bank - merchantId: {merchantId} - paymentTransactionId: {paymentTransactionId} - message: {message}", merchantId, paymentTransactionId, transactionResponse.Message);

                    await this._paymentTransactionService.RejectPaymentTransactionAsync(merchantId, paymentTransactionId, transactionResponse.Message).ConfigureAwait(false);
                }
            }
        }
    }
}
