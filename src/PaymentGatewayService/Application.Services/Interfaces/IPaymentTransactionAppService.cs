﻿
namespace Checkout.PaymentGatewayService.Application.Services.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Checkout.PaymentGatewayService.Application.DTO.Response;

    public interface IPaymentTransactionAppService
    {
        Task<PaymentTransaction> GetPaymentTransactionAsync(string merchantId, Guid paymentTransactionId);

        Task<PaymentTransaction> CreatePaymentTransactionAsync(string merchantId, DTO.Request.PaymentTransaction paymentTransaction);
    }
}
