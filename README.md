# Payment Gateway Service

## Description

The project propose is delivery a payment gateway service to perform transactions between the merchants and the acquiring bank.

The payment-gateway-service was created with microservice concepts using the latest Microsoft .NET Core stacks:

- .NET Core 3.1
- SQL Server 2017
- EntityFrameworkCore
- Docker
- Swagger UI
- HealtCheck UI
- Monitoring and Logging

## Instalation

Please follow the steps below to install the payment-gateway-service on your local environment:

### Software Pre-requirements

- Windows 10
- Visual Studio 2019 Community (or latest)
- Docker Desktop (Linux containers)
- CommandLine and Powershell

### Project Source Code

The payment-gateway-service source code is [hosted on GitLab.com](https://gitlab.com/zuelzer/payment-gateway-service.git), please clone the project for your local environment.

### Installing Project Pre-requirements

After cloning the project: 

- Find the deploy "payment-gateway-service\Deploy" directory into the source project
- Using the Powershell command line, to run the "Run.ps1" file.

The installation file will perform the following tasks:

- Create and initialize a Docker container for Datalust Seq (Log Server)
- Create the local directory "C:\Logs" to storage logs
- Create and initialize a Docker container for SQL Server 2017
- Execute the initials SQL commands to create the project database

The process can probably take some time, as you will need to download the Docker images.

If you get some SQL error during the processes, please run (by Powershell) the "Run.ps1" file once again.

### Starting payment-gateway-service

- Find the deploy "payment-gateway-service\src\PaymentGatewayService" directory into the source project
- Using the Visual Studio, to open the "PaymentGatewayService.sln" solution file.
- Set project "Presentation.WebAPI" as Startup Project
- And run the application on IIS Express mode

## Features (First MVP)

Is supposed the payment-gateway-service starting on port 44337, but you can change it in the project properties (launchSettings.json) if needed.

The following interfaces are available:

### Swagger

[Swagger UI](https://localhost:44337/swagger) - started by default

Through the Swagger UI, you can perform payment requests also read the documentation schema for request and response types.

**GET** `​/PaymentGateway​/v1​/{merchantId}​/PaymentTransactions​/{paymentTransactionId} - Get a payment transaction`

**POST** `​/PaymentGateway​/v1​/{merchantId}​/PaymentTransactions - Get a payment transaction - Create a payment transaction`

### HealtCheck

[HealtCheck](https://localhost:44337/healthcheck) - REST api to check service readiness probes.

[HealtCheck UI](https://localhost:44337/healthchecks-ui) - Web interface to check the health of the service and its dependencies.

[HealtCheck Api](https://localhost:44337/healthchecks-api) - REST Api to check the health of the service and its dependencies.

### Monitoring and Logging

The payment-gateway-service provides many ways to monitoring the application by logging, see below:

[Datalust Seq](http://localhost:5341/): Web interface to analyse and monitoring the application through the application and HTTP logs.

The same logs are available in **Console**, **JSON** and **TXT** files (stored in "C:\Logs" local directory).

## Backlog and Technical Debt

When defining an initial scope for the project, it was necessary to define the delivery priorities covered in the first MVP.

Now we have a backlog product with new features and technical debits that I will describe below:

### Resilience

Add resilience to infrastructure and services dependencies avoiding unavailabilities:

- SQL Server
- Bank Service

### Testing

- Add Performace Tests
- Add Integration Tests
- Improve Unit Tests to coverage 100% of code, currently the code coverage is 75%

### Deployment

- Add Pipelines CI/CD to build the application and run tests
- Add payment-gateway-service running on Docker
- Add code first strategy with the database layer
- Remove SQL Scripts deployment strategy

### API and Security

- Add authentication on payment-gateway-service
- Add authentication on Swagger
- Add authentication between services transactions
- Add data encryption between services transactions