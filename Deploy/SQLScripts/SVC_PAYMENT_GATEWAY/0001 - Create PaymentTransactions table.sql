﻿USE [SVC_PAYMENT_GATEWAY];

IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PaymentTransactions]') AND type in (N'U'))

  BEGIN

    CREATE TABLE [dbo].[PaymentTransactions](
      [Id] [uniqueidentifier] NOT NULL,
      [MerchantId] [varchar](20) NOT NULL,
      [CardNumber] [varchar](50) NOT NULL,
      [CardHolderName] [varchar](50) NULL,
      [CardExpirationMonth] [int] NOT NULL,
      [CardExpirationYear] [int] NOT NULL,
      [CardSecurityCode] [varchar](5) NOT NULL,
      [CurrencyCode] [varchar](3) NOT NULL,
      [Amount] [decimal](9, 2) NOT NULL,
      [Status] [int] NOT NULL,
      [Message] [varchar](255) NULL,
      [DateCreated] [datetime] NOT NULL,
      [DateUpdated] [datetime] NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY])

    ALTER TABLE [dbo].[PaymentTransactions] ADD  DEFAULT (newid()) FOR [Id]

  END