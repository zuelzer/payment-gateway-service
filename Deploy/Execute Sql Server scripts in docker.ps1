Start-Sleep -s 5

$sqlDirectory = "SQLScripts"
$containerSqlServerName = "sql_developer_linux"
$userSa = "sa"
$password = "yourStrong(!)Password"

# Create Cassandra temporary directory
docker exec $containerSqlServerName mkdir -p /tmp/sqlserver/scripts
docker exec $containerSqlServerName bash -c 'rm -rf /tmp/sqlserver/scripts/*.*'

Get-ChildItem $sqlDirectory -Recurse -Filter *.sql | 
Foreach-Object {
    $filename = $_.Name

    Write-Host "Copying filename " $_.FullName " to '${containerSqlServerName}:/tmp/sqlserver/scripts/$filename'".
    docker cp $_.FullName ${containerSqlServerName}:"/tmp/sqlserver/scripts/$filename"

    Write-Host "Executing sqcmd with filname: " $filename
    #execute script in sql server in docker
    docker exec -it $containerSqlServerName /opt/mssql-tools/bin/sqlcmd -S localhost -U $userSa -P $password -i "/tmp/sqlserver/scripts/$filename"
}